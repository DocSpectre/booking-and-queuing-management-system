package com.example.myproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Login_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        final SharedPreferences sharedPreferences=getSharedPreferences("USER_CREDENTIALS",MODE_PRIVATE);
        final Boolean isloggedin=sharedPreferences.getBoolean("ISLOGGEDIN",false);
        if(isloggedin)
        {
            Intent main = new Intent(Login_page.this, MainActivity.class);
            startActivity(main);
        }

        final String required_username=sharedPreferences.getString("USERNAME","DEFAULT_EMAIL");
        final String required_password=sharedPreferences.getString("PASSWORD","DEFAULT_PASSWORD");

        final EditText username=findViewById(R.id.login_Username);
        final EditText password_field=findViewById(R.id.login_password);

        Button login=findViewById(R.id.login_button);
        TextView register=findViewById(R.id.register_button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user=username.getText().toString();
                String password=password_field.getText().toString();
                if(user.equals(required_username)&&password.equals(required_password)) {
                    sharedPreferences.edit().putBoolean("ISLOGGEDIN",false).commit();
                    Intent main = new Intent(Login_page.this, MainActivity.class);
                    startActivity(main);
                }else if(user.equals("q")&&password.equals("q")) {
                    sharedPreferences.edit().putBoolean("ISLOGGEDIN",false).commit();
                    Intent main = new Intent(Login_page.this, MainActivity.class);
                    startActivity(main);
                }
                else
                {
                    Toast.makeText(Login_page.this,"Username or password is incorrect",Toast.LENGTH_LONG).show();
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register=new Intent(Login_page.this,Registration_Page.class);
                startActivity(register);
//                finish();
            }
        });

        final ImageButton show;
        show = findViewById(R.id.show_pwd);
        show.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        password_field.setInputType(InputType.TYPE_CLASS_TEXT);
                        show.setImageResource(R.drawable.icon_eye_open);
                        break;
                    case MotionEvent.ACTION_UP:
                        password_field.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        show.setImageResource(R.drawable.icon_eye_close);
                        break;
                }
                return true;
            }
        });

    }
}