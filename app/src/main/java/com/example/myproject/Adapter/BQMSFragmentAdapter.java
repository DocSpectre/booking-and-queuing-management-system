package com.example.myproject.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myproject.Fragment.PassengerFragment;
import com.example.myproject.Fragment.PaymentFragment;
import com.example.myproject.Fragment.VehicleFragment;

public class BQMSFragmentAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public BQMSFragmentAdapter(FragmentManager fm, int NoofTabs){
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                PassengerFragment passenger = new PassengerFragment();
                return passenger;
            case 1:
                VehicleFragment vehicle = new VehicleFragment();
                return vehicle;
            case 2:
                PaymentFragment payment = new PaymentFragment();
                return payment;
            default:
                return null;
        }
    }
}
