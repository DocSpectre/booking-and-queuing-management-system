package com.example.myproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.myproject.Adapter.BQMSFragmentAdapter;
import com.example.myproject.Fragment.PassengerFragment;
import com.example.myproject.Fragment.PaymentFragment;
import com.example.myproject.Fragment.VehicleFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    Intent logout;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    int total = 0, regularTemp = 0, studentTemp = 0, seniorTemp = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        final SharedPreferences sharedPreferences=getSharedPreferences("USER_CREDENTIALS",MODE_PRIVATE);
        final String name=sharedPreferences.getString("NAME","DEFAULT_NAME");

//        Button logout=findViewById(R.id.menu_logout_button);

        TextView welcometext=findViewById(R.id.welcome_text);
        welcometext.setText("Welcome "+name);

//        login=new Intent(this,Login_page.class);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sharedPreferences.edit().putBoolean("ISLOGGEDIN",false).commit();
//                startActivity(login);
//                finish();
//
//            }
//        });
        dl = findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);

        dl.addDrawerListener(t);
        t.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nv = findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.account:
                        Toast.makeText(MainActivity.this, "My Account",Toast.LENGTH_SHORT).show();break;
                    case R.id.settings:
                        Toast.makeText(MainActivity.this, "Settings",Toast.LENGTH_SHORT).show();break;
                    case R.id.mycart:
                        Toast.makeText(MainActivity.this, "My Cart",Toast.LENGTH_SHORT).show();break;
                    case R.id.logout_button:
                        final SharedPreferences sharedPreferences=getSharedPreferences("USER_CREDENTIALS",MODE_PRIVATE);
                        sharedPreferences.edit().putBoolean("ISLOGGEDIN",false).commit();
                        logout=new Intent(MainActivity.this,Login_page.class);
                        startActivity(logout);
                        finish();
                        Toast.makeText(MainActivity.this, "Logout",Toast.LENGTH_SHORT).show();break;
                    default:
                        return true;
                }
                return true;
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PassengerFragment(), "Passenger");
        adapter.addFragment(new VehicleFragment(), "Vehicle");
        adapter.addFragment(new PaymentFragment(), "Payment");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.header_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.menu_logout_button) {
//            final SharedPreferences sharedPreferences=getSharedPreferences("USER_CREDENTIALS",MODE_PRIVATE);
//            sharedPreferences.edit().putBoolean("ISLOGGEDIN",false).commit();
//            logout=new Intent(this,Login_page.class);
//            startActivity(logout);
//            finish();
//        }
        if(t.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


        public void changeTotal(View view){
            switch (view.getId()) {
                case R.id.reg_btn_add:
                    if (regularTemp < 50){
                    regularTemp++;
                    increase();}
                    break;
                case R.id.stud_btn_add:
                    if (studentTemp < 50){
                    studentTemp++;
                    increase();}
                    break;
                case R.id.senior_btn_add:
                    if (seniorTemp < 50){
                    seniorTemp++;
                    increase();}
                    break;
                case R.id.reg_btn_subtract:
                    if(regularTemp > 0){
                    regularTemp--;
                    decrease();}
                    break;
                case R.id.stud_btn_subtract:
                    if(studentTemp > 0) {
                        studentTemp--;
                        decrease();
                    }
                    break;
                case R.id.senior_btn_subtract:
                    if(seniorTemp > 0){
                    seniorTemp--;
                    decrease();}
                    break;
            }
        }

        public void increase() {
            if (total < 50) {
                total = total + 1;
                display(total);
            }
        }
        public void decrease() {
            if(total > 0) {
                total = total - 1;
                display(total);
            }
        }

        private void display(int totalPassenger) {
            TextView displayTotal = findViewById(R.id.passenger_total);



            displayTotal.setText("" + totalPassenger);
            EditText regularTotal = findViewById(R.id.reg_qty_value);
            regularTotal.setText("" + regularTemp);

            EditText studentTotal = findViewById(R.id.stud_qty_value);
            studentTotal.setText("" + studentTemp);

            EditText seniorTotal = findViewById(R.id.senior_qty_value);
            seniorTotal.setText("" + seniorTemp);
    }
}