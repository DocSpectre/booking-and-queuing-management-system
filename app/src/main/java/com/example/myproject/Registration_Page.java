package com.example.myproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

public class Registration_Page extends AppCompatActivity {
    Intent next_activity, login_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        next_activity = new Intent(this, MainActivity.class);
        login_page = new Intent(this, Login_page.class);
        final EditText firstname_field = findViewById(R.id.firstName);
        final EditText lastname_field = findViewById(R.id.lastName);
        final EditText username = findViewById(R.id.register_username);
        final EditText password_field = findViewById(R.id.Password);
        final EditText confirm_password_field = findViewById(R.id.confirm_password);
        final TextView login_pageBtn = findViewById(R.id.to_login_page);
        Button registerBtn = findViewById(R.id.register);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstname = firstname_field.getText().toString();
                String lastname = lastname_field.getText().toString();
                String user = username.getText().toString();
                String password_1 = password_field.getText().toString();
                String password_2 = confirm_password_field.getText().toString();
                if (password_1.equals(password_2)) {
                    SharedPreferences sharedPreferences = getSharedPreferences("USER_CREDENTIALS", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("NAME", firstname + " " + lastname);
                    editor.putString("USERNAME", user);
                    editor.putString("PASSWORD", password_1);
                    editor.putBoolean("ISLOGGEDIN", true);
                    editor.commit();
                    startActivity(next_activity);
                } else {
                    Toast.makeText(Registration_Page.this, "Passwords don't match", Toast.LENGTH_LONG).show();
                }

            }
        });
        login_pageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(login_page);
//                finish();

            }
        });
    }
}